<div align="center" style="margin-bottom: 10px;">
<span style="color: #6b0f30; font-family: Roboto; font-size: 2rem; font-weight: bold;"> Color-HUH </span>
</div>

<div align="center">
<img src="img/hot.svg" width="140" style="margin-bottom: 10px" alt="">
</div>

Um gerador de cores simples e funcional com JavaScript puro, utilizando conceitos de manipulação de DOM, estrutura de dados, manipulação de cores, responsividade e acessibilidade, espero que gostem
:grin:

<div align="center">

Acesse [aqui](https://jnrgod1.gitlab.io/color-huh/)

</div>

### Sobre o ColorHuH

A interface consiste basicamente em apresentar cores randomicas para o usuário, podendo as mesmas serem modificadas com um simples clique

<img src="img/screens/home.png" alt="">

### Versão

- [ ] 1.0.0
- [x] 1.0.1

### Botões

- Criar box: adiciona uma box com background randomico na DOM
- Criar 10 boxs: adiciona 10 boxs com background randomico
- ColorHuh: Muda o background de todas as boxs presentes na DOM
- Excluir útlima: Remove a ultima box criada
- Limpar tudo: Remove todas as boxs

### Adicionais

- Afim de garantir a legibilidade do texto presente nas boxs, uma função verifica o brilho da caixa e ajusta o texto para escuro ou claro

- Caso queira utilizar alguma cor gerada, é só clicar duas vezes na caixa da cor desejada, que ela é copiada para a área de transferência

### Versão 1.0.1

- Thema Dark/Light adicionado a interface

<img src="img/screens/white.png" style="margin-bottom: 30px" alt="">
<img src="img/screens/dark.png" alt="">

### Contribuição

Caso queira contribuir com o projeto, fique a vontade, toda ajuda e conhecimento são bem vindos :purple_heart:
